package manajemenkos;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

  
public class MyConnection {
    
public Connection createConnection()
            
    {
   
        Connection connection = null;
        MysqlDataSource mds = new MysqlDataSource();
        
        mds.setServerName("localhost");
        mds.setPortNumber(8889);
        mds.setUser("refinaldy");
        mds.setPassword("root");
        mds.setDatabaseName("manajemen_kos");
        mds.setURL("jdbc:mysql://localhost:8889/manajemen_kos?useTimezone=true&serverTimezone=UTC");
        
        try {
            connection = mds.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return connection;
    }
}
